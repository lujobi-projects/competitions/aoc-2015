#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 23;

#[derive(Debug)]
pub enum Instruction {
    Hlf(String),
    Tpl(String),
    Inc(String),
    Jmp(i64),
    Jie(String, i64),
    Jio(String, i64),
}

type Instructions = Vec<Instruction>;

fn run_program(input: Instructions, part2mod: bool) -> i64 {
    let mut registers = HashMap::new();

    if part2mod {
        registers.insert("a".to_string(), 1);
    }

    let ins_len = input.len();
    let mut pc = 0;

    loop {
        if pc >= ins_len {
            break;
        }
        match input[pc] {
            Instruction::Hlf(ref reg) => {
                let val = registers.entry(reg.clone()).or_insert(0);
                *val /= 2;
                pc += 1;
            }
            Instruction::Tpl(ref reg) => {
                let val = registers.entry(reg.clone()).or_insert(0);
                *val *= 3;
                pc += 1;
            }
            Instruction::Inc(ref reg) => {
                let val = registers.entry(reg.clone()).or_insert(0);
                *val += 1;
                pc += 1;
            }
            Instruction::Jmp(offset) => {
                pc = (pc as i64 + offset) as usize;
            }
            Instruction::Jie(ref reg, offset) => {
                let val = registers.entry(reg.clone()).or_insert(0);
                if *val % 2 == 0 {
                    pc = (pc as i64 + offset) as usize;
                } else {
                    pc += 1;
                }
            }
            Instruction::Jio(ref reg, offset) => {
                let val = registers.entry(reg.clone()).or_insert(0);
                if *val == 1 {
                    pc = (pc as i64 + offset) as usize;
                } else {
                    pc += 1;
                }
            }
        }
    }
    *registers.get("b").unwrap_or(&0) as i64
}

pub fn part1() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();
    run_program(input, false)
}

pub fn part2() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();
    run_program(input, true)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 184);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 231);
    }
}
