use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 24;

fn subdivide(input: Vec<i64>, divisions: i64) -> i64 {
    let weight = input.iter().sum::<i64>() / divisions;
    let max_ct = 7;

    (1..max_ct)
        .map(|ct| {
            input
                .clone()
                .into_iter()
                .combinations(ct)
                .filter(|c| c.iter().sum::<i64>() == weight)
                .map(|comb| comb.into_iter().product::<i64>())
                .min()
                .unwrap_or(i64::MAX)
        })
        .min()
        .unwrap()
}

pub fn part1() -> i64 {
    subdivide(read_input!("\n", i64), 3)
}

pub fn part2() -> i64 {
    subdivide(read_input!("\n", i64), 4)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 10439961859);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 72050269);
    }
}
