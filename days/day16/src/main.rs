use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 16;

pub fn part1() -> i64 {
    let input = read_input!('\n', ' ', String);

    let aunts = input.iter().map(|line| {
        let mut aunt = HashMap::new();
        let mut line = line.iter();
        line.next();
        line.next();
        while let Some(key) = line.next() {
            let value = line
                .next()
                .unwrap()
                .trim_end_matches(',')
                .parse::<u64>()
                .unwrap();
            aunt.insert(key.to_string().trim_end_matches(':').to_string(), value);
        }
        aunt
    });

    aunts
        .enumerate()
        .find(|x| {
            let aunt = x.clone().1;
            aunt.get("children").map_or(true, |x| x == &3)
                && aunt.get("cats").map_or(true, |x| x == &7)
                && aunt.get("samoyeds").map_or(true, |x| x == &2)
                && aunt.get("pomeranians").map_or(true, |x| x == &3)
                && aunt.get("akitas").map_or(true, |x| x == &0)
                && aunt.get("vizslas").map_or(true, |x| x == &0)
                && aunt.get("goldfish").map_or(true, |x| x == &5)
                && aunt.get("trees").map_or(true, |x| x == &3)
                && aunt.get("cars").map_or(true, |x| x == &2)
                && aunt.get("perfumes").map_or(true, |x| x == &1)
        })
        .unwrap()
        .0 as i64
        + 1
    // 0
}

pub fn part2() -> i64 {
    let input = read_input!('\n', ' ', String);

    let aunts = input.iter().map(|line| {
        let mut aunt = HashMap::new();
        let mut line = line.iter();
        line.next();
        line.next();
        while let Some(key) = line.next() {
            let value = line
                .next()
                .unwrap()
                .trim_end_matches(',')
                .parse::<u64>()
                .unwrap();
            aunt.insert(key.to_string().trim_end_matches(':').to_string(), value);
        }
        aunt
    });

    aunts
        .enumerate()
        .find(|x| {
            let aunt = x.clone().1;
            aunt.get("children").map_or(true, |x| x == &3)
                && aunt.get("cats").map_or(true, |x| x > &7)
                && aunt.get("samoyeds").map_or(true, |x| x == &2)
                && aunt.get("pomeranians").map_or(true, |x| x < &3)
                && aunt.get("akitas").map_or(true, |x| x == &0)
                && aunt.get("vizslas").map_or(true, |x| x == &0)
                && aunt.get("goldfish").map_or(true, |x| x < &5)
                && aunt.get("trees").map_or(true, |x| x > &3)
                && aunt.get("cars").map_or(true, |x| x == &2)
                && aunt.get("perfumes").map_or(true, |x| x == &1)
        })
        .unwrap()
        .0 as i64
        + 1
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 373);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 260);
    }
}
