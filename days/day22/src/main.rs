use utils::*;

const CURRENT_DAY: u8 = 22;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum Spell {
    MagicMissile,
    Drain,
    Shield,
    Poison,
    Recharge,
}

#[derive(Debug, Clone, Copy)]
struct Character {
    hp: i64,
    armor: i64,
    mana: i64,
    shield_effect: i64,
    poison_effect: i64,
    recharge_effect: i64,
}

impl Character {
    const fn new(hp: i64, armor: i64, mana: i64) -> Character {
        Character {
            hp,
            armor,
            mana,
            shield_effect: 0,
            poison_effect: 0,
            recharge_effect: 0,
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Boss {
    hp: i64,
    damage: i64,
}

impl Boss {
    const fn new(hp: i64, damage: i64) -> Boss {
        Boss { hp, damage }
    }
}

const SPELLS: [Spell; 5] = [
    Spell::MagicMissile,
    Spell::Drain,
    Spell::Shield,
    Spell::Poison,
    Spell::Recharge,
];

fn wrapped_play_all_games(player: Character, boss: Boss, hard_mode: bool) -> (i64, Vec<Spell>) {
    let mut c = i64::MAX / 3;
    play_all_games(player, boss, hard_mode, 0, 0, &mut c)
}

// TODO: this should not be needed
const MAX_DEPTH: i64 = 8;

fn play_all_games(
    player: Character,
    boss: Boss,
    hard_mode: bool,
    depth: i64,
    curr_cost: i64,
    curr_overall_min_cost: &mut i64,
) -> (i64, Vec<Spell>) {
    let mut min_cost = i64::MAX / 3;
    let mut min_spell = vec![];

    for spell in SPELLS.iter() {
        if depth > MAX_DEPTH {
            continue;
        }
        let mut player = player;
        let mut boss = boss;

        if hard_mode {
            player.hp -= 1;
            if player.hp <= 0 {
                continue;
            }
        }

        if (player.shield_effect > 1 && spell == &Spell::Shield)
            || (player.poison_effect > 1 && spell == &Spell::Poison)
            || (player.recharge_effect > 1 && spell == &Spell::Recharge)
        {
            continue;
        }

        if player.shield_effect > 0 {
            player.armor = 7;
        } else {
            player.armor = 0;
        }
        if player.recharge_effect > 0 {
            player.mana += 101;
        }
        if player.poison_effect > 0 {
            boss.hp -= 3;
            if boss.hp <= 0 {
                return (0, vec![]);
            }
        }

        player.poison_effect = (player.poison_effect - 1).max(0);
        player.recharge_effect = (player.recharge_effect - 1).max(0);
        player.shield_effect = (player.shield_effect - 1).max(0);

        let cost = match spell {
            Spell::MagicMissile => 53,
            Spell::Drain => 73,
            Spell::Shield => 113,
            Spell::Poison => 173,
            Spell::Recharge => 229,
        };

        if cost + curr_cost > *curr_overall_min_cost {
            continue;
        }

        player.mana -= cost;

        if player.mana < 0 {
            continue;
        }

        match spell {
            Spell::MagicMissile => boss.hp -= 4,
            Spell::Drain => {
                boss.hp -= 2;
                player.hp += 2;
            }
            Spell::Shield => player.shield_effect = 6,
            Spell::Poison => player.poison_effect = 6,
            Spell::Recharge => player.recharge_effect = 5,
        }

        if boss.hp <= 0 {
            if min_cost > cost {
                min_cost = cost;
                min_spell = vec![spell.clone()];
            }
            continue;
        }

        // Boss turn
        if player.shield_effect > 0 {
            player.armor = 7;
        } else {
            player.armor = 0;
        }
        if player.recharge_effect > 0 {
            player.mana += 101;
        }
        if player.poison_effect > 0 {
            boss.hp -= 3;
            if boss.hp <= 0 && min_cost > cost {
                min_cost = cost;
                min_spell = vec![spell.clone()];
            }
        }
        player.poison_effect = (player.poison_effect - 1).max(0);
        player.recharge_effect = (player.recharge_effect - 1).max(0);
        player.shield_effect = (player.shield_effect - 1).max(0);

        player.hp -= (boss.damage - player.armor).max(1);

        if player.hp <= 0 {
            continue;
        }

        let (new_cost, mut new_spells) = play_all_games(
            player,
            boss,
            hard_mode,
            depth + 1,
            curr_cost + cost,
            curr_overall_min_cost,
        );
        let new_cost = new_cost + cost;
        if min_cost > new_cost {
            min_cost = new_cost;
            new_spells.insert(0, spell.clone());
            min_spell = new_spells;
        }
    }
    *curr_overall_min_cost = (*curr_overall_min_cost).min(min_cost);
    (min_cost, min_spell)
}

pub fn part1() -> i64 {
    let input = get_input!();

    let binding = input
        .lines()
        .map(|l| {
            let mut parts = l.split(": ");
            let _ = parts.next().unwrap();
            parts.next().unwrap().parse::<i64>().unwrap()
        })
        .collect::<Vec<_>>();
    let [hp, damage ] = binding
        .as_slice() else {panic!("Invalid input")};

    const PLAYER: Character = Character::new(50, 0, 500);
    // lower than 1387
    // [Poison, MagicMissile, Recharge, Poison, Shield, Recharge, Poison, Drain, MagicMissile]
    let a = wrapped_play_all_games(PLAYER, Boss::new(*hp, *damage), false);
    // println!("{:?}", a.1);
    a.0
}

pub fn part2() -> i64 {
    let input = get_input!();

    let binding = input
        .lines()
        .map(|l| {
            let mut parts = l.split(": ");
            let _ = parts.next().unwrap();
            parts.next().unwrap().parse::<i64>().unwrap()
        })
        .collect::<Vec<_>>();
    let [hp, damage ] = binding
        .as_slice() else {panic!("Invalid input")};
    const PLAYER: Character = Character::new(50, 0, 500);

    let a = wrapped_play_all_games(PLAYER, Boss::new(*hp, *damage), true);
    // println!("{:?}", a.1);
    a.0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1269);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1309);
    }
}
