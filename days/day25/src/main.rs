use utils::*;

const CURRENT_DAY: u8 = 25;

pub fn part1() -> i64 {
    let input = read_input!(" ", String);
    let row = input[16].replace(',', "").parse::<u64>().unwrap();
    let column = input[18].replace('.', "").parse::<u64>().unwrap();

    let modulo = 33554393_u64;
    let multipy = 252533_u64;

    let target_index = (((row + column - 1) * (row + column - 1) + row + column - 1) / 2)
        - ((row + column - 1) - column);

    let mut code = 20151125;

    for _ in 1..target_index {
        code = (code * multipy) % modulo;
    }
    code as i64
}
pub fn part2() -> i64 {
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 9132360);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 0);
    }
}
