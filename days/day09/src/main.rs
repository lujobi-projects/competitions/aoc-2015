#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::{cmp::max, cmp::min, collections::HashSet};

use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 9;

#[derive(Debug)]
pub struct Edge {
    pub start: String,
    pub end: String,
    pub dist: u64,
}

fn traveling_salesman(edges: &Vec<Edge>) -> u64 {
    let mut cities = HashSet::new();
    for edge in edges {
        cities.insert(edge.start.clone());
        cities.insert(edge.end.clone());
    }

    let mut min_dist = u64::MAX;
    for perm in cities.iter().permutations(cities.len()) {
        let mut dist = 0;
        for i in 0..perm.len() - 1 {
            let start = &perm[i];
            let end = &perm[i + 1];
            let edge = edges
                .iter()
                .find(|e| {
                    (e.start == **start && e.end == **end) || (e.start == **end && e.end == **start)
                })
                .unwrap();
            dist += edge.dist;
        }
        min_dist = min(min_dist, dist);
    }
    min_dist
}

fn rev_traveling_salesman(edges: &Vec<Edge>) -> u64 {
    let mut cities = HashSet::new();
    for edge in edges {
        cities.insert(edge.start.clone());
        cities.insert(edge.end.clone());
    }

    let mut max_dist = u64::MIN;
    for perm in cities.iter().permutations(cities.len()) {
        let mut dist = 0;
        for i in 0..perm.len() - 1 {
            let start = &perm[i];
            let end = &perm[i + 1];
            let edge = edges
                .iter()
                .find(|e| {
                    (e.start == **start && e.end == **end) || (e.start == **end && e.end == **start)
                })
                .unwrap();
            dist += edge.dist;
        }
        max_dist = max(max_dist, dist);
    }
    max_dist
}

pub fn part1() -> i64 {
    let input = parser::EdgesParser::new().parse(&get_input!()).unwrap();
    traveling_salesman(&input) as i64
}

pub fn part2() -> i64 {
    let input = parser::EdgesParser::new().parse(&get_input!()).unwrap();
    rev_traveling_salesman(&input) as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 141);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 736);
    }
}
