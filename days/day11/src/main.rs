use utils::*;

const CURRENT_DAY: u8 = 11;

fn increase_string(s: String) -> String {
    let mut chars = s.chars().collect::<Vec<char>>();
    let mut i = chars.len() - 1;
    loop {
        if chars[i] == 'z' {
            chars[i] = 'a';
            if i == 0 {
                chars.insert(0, 'a');
                break;
            }
            i -= 1;
        } else {
            chars[i] = (chars[i] as u8 + 1) as char;
            break;
        }
    }
    chars.into_iter().collect()
}

fn find_passwd(mut new_passwd: String) -> String {
    loop {
        new_passwd = increase_string(new_passwd);
        let mut has_straight = false;
        let mut has_pairs = 0;
        let mut last_char = new_passwd.chars().next().unwrap();
        let mut pair_chars = Vec::new();
        for c in new_passwd.chars().skip(1) {
            if c == last_char {
                if !pair_chars.contains(&c) {
                    pair_chars.push(c);
                    has_pairs += 1;
                }
            } else if c as u8 == last_char as u8 + 1 && !has_straight {
                if let Some(next_char) = new_passwd
                    .chars()
                    .nth(new_passwd.chars().position(|x| x == c).unwrap() + 1)
                {
                    if next_char as u8 == c as u8 + 1 {
                        has_straight = true;
                    }
                }
            }

            last_char = c;
        }
        if has_straight
            && has_pairs >= 2
            && !new_passwd.contains('i')
            && !new_passwd.contains('o')
            && !new_passwd.contains('l')
        {
            return new_passwd;
        }
    }
}

pub fn part1() -> String {
    let input = get_input!();
    find_passwd(input)
}
pub fn part2() -> String {
    let input = get_input!();
    find_passwd(find_passwd(input))
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), "vzbxxyzz");
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), "vzcaabcc");
    }
}
