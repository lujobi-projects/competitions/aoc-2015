use utils::*;

const CURRENT_DAY: u8 = 18;

const CONWAY_STEPS: u8 = 100;

fn conway(grid: &Vec<Vec<bool>>) -> Vec<Vec<bool>> {
    let mut new_grid = grid.clone();
    for (y, row) in grid.iter().enumerate() {
        for (x, cell) in row.iter().enumerate() {
            let mut neighbors = 0;
            for (dy, dx) in &[
                (-1, -1),
                (-1, 0),
                (-1, 1),
                (0, -1),
                (0, 1),
                (1, -1),
                (1, 0),
                (1, 1),
            ] {
                if y as i64 + dy < 0 || y as i64 + dy >= grid.len() as i64 {
                    continue;
                }
                if x as i64 + dx < 0 || x as i64 + dx >= row.len() as i64 {
                    continue;
                }
                if grid[(y as i64 + dy) as usize][(x as i64 + dx) as usize] {
                    neighbors += 1;
                }
            }
            if *cell && neighbors != 2 && neighbors != 3 {
                new_grid[y][x] = false;
            } else if neighbors == 3 {
                new_grid[y][x] = true;
            }
        }
    }
    new_grid
}

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    let mut grid = input
        .iter()
        .map(|s| s.chars().map(|c| c == '#').collect::<Vec<_>>())
        .collect::<Vec<_>>();

    for _ in 0..CONWAY_STEPS {
        grid = conway(&grid);
    }

    grid.iter()
        .map(|row| row.iter().filter(|&&b| b).count())
        .sum::<usize>() as i64
}
pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    let mut grid = input
        .iter()
        .map(|s| s.chars().map(|c| c == '#').collect::<Vec<_>>())
        .collect::<Vec<_>>();

    for _ in 0..CONWAY_STEPS {
        grid = conway(&grid);
        grid[0][0] = true;
        grid[0][99] = true;
        grid[99][0] = true;
        grid[99][99] = true;
    }

    grid.iter()
        .map(|row| row.iter().filter(|&&b| b).count())
        .sum::<usize>() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 768);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 781);
    }
}
