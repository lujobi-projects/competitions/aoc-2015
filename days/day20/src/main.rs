use std::str::FromStr;
use utils::*;

const CURRENT_DAY: u8 = 20;

pub fn part1() -> i64 {
    let input = u64::from_str(get_input!().as_str()).unwrap() / 10;

    let mut houses = vec![0; input as usize];
    let mut housenumber = input;

    for elf in 1..input {
        for house in (elf..input).step_by(elf as usize) {
            houses[house as usize] += elf;
            if houses[house as usize] >= input && house < housenumber {
                housenumber = house;
            }
        }
    }
    housenumber as i64
}
pub fn part2() -> i64 {
    let input = u64::from_str(get_input!().as_str()).unwrap() / 10;

    let mut houses = vec![0; input as usize];
    let mut housenumber = input;

    for elf in 1..input {
        for house in (elf..input.min(elf + elf * 50)).step_by(elf as usize) {
            houses[house as usize] += elf * 11;
            if houses[house as usize] >= input * 10 && house < housenumber {
                housenumber = house;
            }
        }
    }
    housenumber as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 776160);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 786240);
    }
}
