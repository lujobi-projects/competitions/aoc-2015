#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashSet;

use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 13;

#[derive(Debug)]
pub struct Happyness {
    person: String,
    neighbour: String,
    happyness: i32,
}

fn calc_max_happyness(input: &[Happyness]) -> i64 {
    let persons = input
        .iter()
        .map(|h| h.person.clone())
        .collect::<HashSet<_>>();
    let plen = persons.len();

    // TODO: suboptimal solution, but it works
    // Djikstra's algorithm would or similar would be better

    let perms = persons.into_iter().permutations(plen);
    perms
        .map(|p| {
            let mut happyness = 0;
            for i in 0..p.len() {
                let left = if i == 0 { p.len() - 1 } else { i - 1 };
                let right = if i == p.len() - 1 { 0 } else { i + 1 };
                happyness += input
                    .iter()
                    .filter(|h| h.person == p[i] && h.neighbour == p[left])
                    .map(|h| h.happyness)
                    .sum::<i32>();

                happyness += input
                    .iter()
                    .filter(|h| h.person == p[i] && h.neighbour == p[right])
                    .map(|h| h.happyness)
                    .sum::<i32>();
            }
            happyness
        })
        .max()
        .unwrap() as i64
}

pub fn part1() -> i64 {
    let input = parser::HappynessListParser::new()
        .parse(&get_input!())
        .unwrap();
    calc_max_happyness(&input)
}

pub fn part2() -> i64 {
    let input = parser::HappynessListParser::new()
        .parse(&get_input!())
        .unwrap();
    let mut input = input;
    let persons = input
        .iter()
        .map(|h| h.person.clone())
        .collect::<HashSet<_>>();
    for p in persons {
        input.push(Happyness {
            person: "me".to_string(),
            neighbour: p.clone(),
            happyness: 0,
        });
        input.push(Happyness {
            person: p,
            neighbour: "me".to_string(),
            happyness: 0,
        });
    }
    calc_max_happyness(&input)
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 618);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 601);
    }
}
