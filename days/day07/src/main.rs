#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 7;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Instruction {
    Set(Signal),
    And(Signal, Signal),
    Or(Signal, Signal),
    Not(Signal),
    Lshift(Signal, Signal),
    Rshift(Signal, Signal),
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum Signal {
    Wire(String),
    Value(u16),
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Expression(pub Instruction, pub String);

type Circuit<'a> = HashMap<String, &'a Expression>;
type CircuitCache = HashMap<String, u16>;

fn follow_operand(operand: &Signal, wires: &Circuit, cache: &mut CircuitCache) -> u16 {
    match operand {
        Signal::Wire(cable) => {
            if let Some(value) = cache.get(cable) {
                *value
            } else {
                let value = follow_wire(cable.to_string(), wires, cache);
                cache.insert(cable.clone(), value);
                value
            }
        }
        Signal::Value(value) => *value,
    }
}

fn follow_wire(c: String, wires: &Circuit, cache: &mut CircuitCache) -> u16 {
    match wires.get(&c) {
        Some(&Expression(ref i, ref c)) => {
            let v = match *i {
                Instruction::Set(ref o) => follow_operand(o, wires, cache),
                Instruction::And(ref o1, ref o2) => {
                    follow_operand(o1, wires, cache) & follow_operand(o2, wires, cache)
                }
                Instruction::Or(ref o1, ref o2) => {
                    follow_operand(o1, wires, cache) | follow_operand(o2, wires, cache)
                }
                Instruction::Not(ref o) => !follow_operand(o, wires, cache),
                Instruction::Lshift(ref o1, ref o2) => {
                    follow_operand(o1, wires, cache) << follow_operand(o2, wires, cache)
                }
                Instruction::Rshift(ref o1, ref o2) => {
                    follow_operand(o1, wires, cache) >> follow_operand(o2, wires, cache)
                }
            };
            cache.insert(c.clone(), v);
            v
        }
        None => panic!("No such wire: {}", c),
    }
}

pub fn part1() -> i64 {
    let input = parser::ExpressionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let mut wires: Circuit = HashMap::new();
    let mut cache: CircuitCache = HashMap::new();
    input.iter().for_each(|wire| {
        wires.insert(wire.1.clone(), wire);
    });

    follow_wire("a".to_string(), &wires, &mut cache) as i64
}

pub fn part2() -> i64 {
    let input = parser::ExpressionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let mut _wires: Circuit = HashMap::new();
    input.iter().for_each(|wire| {
        _wires.insert(wire.1.clone(), wire);
    });

    let wires = _wires.clone();
    let mut cache: CircuitCache = HashMap::new();

    let a_res = follow_wire("a".to_string(), &wires, &mut cache);

    let b_cable = "b".to_string();
    let new_b_expr = Expression(Instruction::Set(Signal::Value(a_res)), b_cable.clone());
    _wires.insert(b_cable, &new_b_expr);
    let wires = _wires;
    let mut cache: CircuitCache = HashMap::new();

    follow_wire("a".to_string(), &wires, &mut cache) as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 3176);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 14710);
    }
}
