use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = read_input!();
    input.len() as i64 - 2 * input.iter().filter(|x| **x == ')').count() as i64
}
pub fn part2() -> i64 {
    let input = read_input!();
    let mut floor = 0;
    for (i, c) in input.iter().enumerate() {
        floor += if *c == '(' { 1 } else { -1 };
        if floor == -1 {
            return i as i64 + 1;
        }
    }
    0
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 280);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1797);
    }
}
