use utils::*;

const CURRENT_DAY: u8 = 21;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Tool<'a> {
    name: &'a str,
    cost: i64,
    damage: i64,
    armor: i64,
}

impl Tool<'_> {
    const fn new(name: &str, cost: i64, damage: i64, armor: i64) -> Tool {
        Tool {
            name,
            cost,
            damage,
            armor,
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Character {
    hp: i64,
    damage: i64,
    armor: i64,
}

impl Character {
    const fn new(hp: i64, damage: i64, armor: i64) -> Character {
        Character { hp, damage, armor }
    }
}

const WEAPONS: [Tool; 5] = [
    Tool::new("Dagger", 8, 4, 0),
    Tool::new("Shortsword", 10, 5, 0),
    Tool::new("Warhammer", 25, 6, 0),
    Tool::new("Longsword", 40, 7, 0),
    Tool::new("Greataxe", 74, 8, 0),
];

const ARMOR: [Tool; 6] = [
    Tool::new("Leather", 13, 0, 1),
    Tool::new("Chainmail", 31, 0, 2),
    Tool::new("Splintmail", 53, 0, 3),
    Tool::new("Bandedmail", 75, 0, 4),
    Tool::new("Platemail", 102, 0, 5),
    Tool::new("No armor", 0, 0, 0),
];

const RINGS: [Tool; 8] = [
    Tool::new("Damage +1", 25, 1, 0),
    Tool::new("Damage +2", 50, 2, 0),
    Tool::new("Damage +3", 100, 3, 0),
    Tool::new("Defense +1", 20, 0, 1),
    Tool::new("Defense +2", 40, 0, 2),
    Tool::new("Defense +3", 80, 0, 3),
    Tool::new("No ring 1", 0, 0, 0),
    Tool::new("No ring 2", 0, 0, 0),
];

fn player_wins(player: Character, boss: Character) -> bool {
    let mut player = player;
    let mut boss = boss;

    loop {
        boss.hp -= player.damage - boss.armor;
        if boss.hp <= 0 {
            return true;
        }

        player.hp -= boss.damage - player.armor;
        if player.hp <= 0 {
            return false;
        }
    }
}

pub fn part1() -> i64 {
    let input = get_input!();

    let binding = input
        .lines()
        .map(|l| {
            let mut parts = l.split(": ");
            let _ = parts.next().unwrap();
            parts.next().unwrap().parse::<i64>().unwrap()
        })
        .collect::<Vec<_>>();
    let [hp, damage, armor] = binding
        .as_slice() else {panic!("Invalid input")};

    let boss = Character::new(*hp, *damage, *armor);

    let mut min_cost = i64::max_value();

    for weapon in &WEAPONS {
        for armor in &ARMOR {
            for ring1 in &RINGS {
                for ring2 in &RINGS {
                    if ring1.name == ring2.name {
                        continue;
                    }

                    let cost = weapon.cost + armor.cost + ring1.cost + ring2.cost;
                    let damage = weapon.damage + armor.damage + ring1.damage + ring2.damage;
                    let armor = weapon.armor + armor.armor + ring1.armor + ring2.armor;

                    if player_wins(Character::new(100, damage, armor), boss) {
                        min_cost = min_cost.min(cost);
                    }
                }
            }
        }
    }

    min_cost
}
pub fn part2() -> i64 {
    let input = get_input!();

    let binding = input
        .lines()
        .map(|l| {
            let mut parts = l.split(": ");
            let _ = parts.next().unwrap();
            parts.next().unwrap().parse::<i64>().unwrap()
        })
        .collect::<Vec<_>>();
    let [hp, damage, armor] = binding
        .as_slice() else {panic!("Invalid input")};

    let boss = Character::new(*hp, *damage, *armor);

    let mut max_cost = 0;

    for weapon in &WEAPONS {
        for armor in &ARMOR {
            for ring1 in &RINGS {
                for ring2 in &RINGS {
                    if ring1.name == ring2.name {
                        continue;
                    }

                    let cost = weapon.cost + armor.cost + ring1.cost + ring2.cost;
                    let damage = weapon.damage + armor.damage + ring1.damage + ring2.damage;
                    let armor = weapon.armor + armor.armor + ring1.armor + ring2.armor;

                    if !player_wins(Character::new(100, damage, armor), boss) {
                        max_cost = max_cost.max(cost);
                    }
                }
            }
        }
    }

    max_cost
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 111);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 188);
    }
}
