#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use std::collections::HashMap;

use utils::*;

const CURRENT_DAY: u8 = 12;

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    String(String),
    Number(i32),
    Array(Vec<Value>),
    Object(HashMap<String, Value>),
}

fn sum_numbers(value: &Value) -> i32 {
    match value {
        Value::Number(n) => *n,
        Value::Array(a) => a.iter().map(sum_numbers).sum(),
        Value::Object(o) => o.values().map(sum_numbers).sum(),
        _ => 0,
    }
}

fn sum_numbers_without_red(value: &Value) -> i32 {
    match value {
        Value::Number(n) => *n,
        Value::Array(a) => a.iter().map(sum_numbers_without_red).sum(),
        Value::Object(o) => {
            if o.values().any(|x| *x == Value::String("red".to_string())) {
                0
            } else {
                o.values().map(sum_numbers_without_red).sum()
            }
        }
        _ => 0,
    }
}

pub fn part1() -> i64 {
    let input = parser::ValueParser::new().parse(&get_input!()).unwrap();
    sum_numbers(&input) as i64
}

pub fn part2() -> i64 {
    let input = parser::ValueParser::new().parse(&get_input!()).unwrap();
    sum_numbers_without_red(&input) as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 156366);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 96852);
    }
}
