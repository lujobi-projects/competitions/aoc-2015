use std::collections::HashSet;

use regex::Regex;
use utils::*;

const CURRENT_DAY: u8 = 19;

pub fn part1() -> i64 {
    let mut input = read_input!("\n", String);
    let molecule = input.remove(input.len() - 1);
    input.remove(input.len() - 1);

    let mut mutations = Vec::new();
    for line in input {
        let splitted = line
            .split_whitespace()
            .map(String::from)
            .collect::<Vec<_>>();
        mutations.push((splitted[0].clone(), splitted[2].clone()));
    }

    let mut mutated_molecules = HashSet::new();

    for mutation in mutations {
        let reg = Regex::new(mutation.0.as_str()).unwrap();
        for m in reg.find_iter(molecule.as_str()) {
            let mol = format!(
                "{}{}{}",
                &molecule[0..m.start()],
                mutation.1,
                &molecule[m.end()..]
            );
            mutated_molecules.insert(mol);
        }
    }
    mutated_molecules.len() as i64
}

pub fn part2() -> i64 {
    let mut input = read_input!("\n", String);

    let molecule = input.remove(input.len() - 1);
    input.remove(input.len() - 1);

    let mut mutations = Vec::new();
    for line in input {
        let splitted = line
            .split_whitespace()
            .map(String::from)
            .collect::<Vec<_>>();
        mutations.push((splitted[0].clone(), splitted[2].clone()));
    }

    // Todo: could be done without array
    mutations.sort_by(|x, y| y.1.len().cmp(&x.1.len()));

    let mut molecules = vec![molecule];

    let mut counter = 0;
    while !molecules.contains(&String::from("e")) {
        counter += 1;
        let mut new_molecules = Vec::new();
        for molecule in molecules {
            for mutation in mutations.iter().rev() {
                let reg = Regex::new(mutation.1.as_str()).unwrap();
                for m in reg.find_iter(molecule.as_str()) {
                    let mol = format!(
                        "{}{}{}",
                        &molecule[0..m.start()],
                        mutation.0,
                        &molecule[m.end()..]
                    );
                    new_molecules.push(mol);
                }
            }
        }
        new_molecules.sort_by_key(|x| x.len());
        molecules = new_molecules[0..1].to_vec();
    }
    counter
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 576);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 207);
    }
}
