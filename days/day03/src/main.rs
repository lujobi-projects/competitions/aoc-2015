use std::collections::HashSet;

use utils::*;

const CURRENT_DAY: u8 = 1;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Coordinates {
    x: i32,
    y: i32,
}

pub fn part1() -> i64 {
    let input = read_input!();
    let mut curr_coords = Coordinates { x: 0, y: 0 };
    let mut visited = HashSet::new();
    visited.insert(curr_coords);
    for c in input {
        match c {
            '^' => curr_coords.y += 1,
            'v' => curr_coords.y -= 1,
            '>' => curr_coords.x += 1,
            '<' => curr_coords.x -= 1,
            _ => panic!("Invalid input"),
        }
        visited.insert(curr_coords);
    }
    visited.len() as i64
}

pub fn part2() -> i64 {
    let input = read_input!();
    // let input = debug_input!("^>v<");
    let mut curr_santa_coords = Coordinates { x: 0, y: 0 };
    let mut curr_robo_coords = Coordinates { x: 0, y: 0 };
    let mut visited = HashSet::new();
    visited.insert(curr_santa_coords);
    for (i, c) in input.iter().enumerate() {
        let mut curr_coords = if i % 2 == 0 {
            &mut curr_santa_coords
        } else {
            &mut curr_robo_coords
        };
        match c {
            '^' => curr_coords.y += 1,
            'v' => curr_coords.y -= 1,
            '>' => curr_coords.x += 1,
            '<' => curr_coords.x -= 1,
            _ => panic!("Invalid input"),
        }
        visited.insert(*curr_coords);
    }
    visited.len() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 2081);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 2341);
    }
}
