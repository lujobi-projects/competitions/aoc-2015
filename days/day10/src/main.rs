use utils::*;

const CURRENT_DAY: u8 = 10;

// fn count_cached("")

pub fn part1() -> i64 {
    let input = get_input!();

    let mut say = input;

    for _ in 0..40 {
        let mut new_say = String::new();
        let mut last_char = say.chars().next().unwrap();
        let mut count = 1;
        for c in say.chars().skip(1) {
            if c == last_char {
                count += 1;
            } else {
                new_say.push_str(&count.to_string());
                new_say.push(last_char);
                last_char = c;
                count = 1;
            }
        }
        new_say.push_str(&count.to_string());
        new_say.push(last_char);
        say = new_say;
    }

    say.len() as i64
}

pub fn part2() -> i64 {
    let input = get_input!();

    // TODO crappy solution, but it works
    // Could be optimized by caching the results of the previous iterations

    let mut say = input;

    for _ in 0..50 {
        let mut new_say = String::new();
        let mut last_char = say.chars().next().unwrap();
        let mut count = 1;
        for c in say.chars().skip(1) {
            if c == last_char {
                count += 1;
            } else {
                new_say.push_str(&count.to_string());
                new_say.push(last_char);
                last_char = c;
                count = 1;
            }
        }
        new_say.push_str(&count.to_string());
        new_say.push(last_char);
        say = new_say;
    }

    say.len() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 360154);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 5103798);
    }
}
