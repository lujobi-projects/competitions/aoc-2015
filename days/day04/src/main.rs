use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = get_input!();
    // let input = "abcdef";

    let mut count = 346000;

    loop {
        let hash = md5::compute(format!("{}{}", input, count));
        let hash = format!("{:x}", hash);
        if hash.starts_with("00000") {
            return count;
        }
        count += 1;
    }
}

pub fn part2() -> i64 {
    let input = get_input!();
    // let input = "abcdef";

    let mut count = 9958000; // to speed up testing

    loop {
        let hash = md5::compute(format!("{}{}", input, count));
        let hash = format!("{:x}", hash);
        if hash.starts_with("000000") {
            return count;
        }
        count += 1;
    }
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 346386);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 9958218);
    }
}
