#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 14;

const RUNNING_TIME: i32 = 2503;

#[derive(Debug)]
pub struct ReindeerConfig {
    speed: i32,
    duration: i32,
    rest: i32,
}

pub fn part1() -> i64 {
    let input = parser::ReindeerListParser::new()
        .parse(&get_input!())
        .unwrap();
    input
        .iter()
        .map(|r| {
            let mut distance = 0;
            let mut time = 0;

            while time < RUNNING_TIME {
                distance += r.speed * r.duration.min(2503 - time);
                time += r.duration + r.rest;
            }
            distance
        })
        .max()
        .unwrap() as i64
}

pub fn part2() -> i64 {
    let input = parser::ReindeerListParser::new()
        .parse(&get_input!())
        .unwrap();
    let mut points = vec![0; input.len()];
    for time in 1..=RUNNING_TIME {
        let mut distances = vec![];
        for r in input.iter() {
            let mut distance = 0;
            let mut t = 0;
            while t < time {
                distance += r.speed * r.duration.min(time - t);
                t += r.duration + r.rest;
            }
            distances.push(distance);
        }
        let max_distance = distances.iter().max().unwrap();
        for (i, d) in distances.iter().enumerate() {
            if d == max_distance {
                points[i] += 1;
            }
        }
    }
    *points.iter().max().unwrap() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 2655);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 1059);
    }
}
