use itertools::Itertools;
use utils::*;

const CURRENT_DAY: u8 = 17;

pub fn part1() -> i64 {
    let buckets = read_input!("\n", u64);
    let mut combinations = 0;

    for i in 1..buckets.len() {
        for combination in buckets.iter().combinations(i) {
            if combination.into_iter().sum::<u64>() == 150 {
                combinations += 1;
            }
        }
    }
    combinations
}
pub fn part2() -> i64 {
    let buckets = read_input!("\n", u64);

    let mut combinations = 0;

    for combination in buckets.iter().combinations(4) {
        if combination.into_iter().sum::<u64>() == 150 {
            combinations += 1;
        }
    }
    combinations
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 654);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 57);
    }
}
