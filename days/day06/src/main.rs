#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 6;

#[derive(Debug)]
pub enum Action {
    TurnOn,
    TurnOff,
    Toggle,
}

#[derive(Debug)]
pub struct Coordinate {
    x: u64,
    y: u64,
}

#[derive(Debug)]
pub struct Instruction {
    action: Action,
    start: Coordinate,
    end: Coordinate,
}

pub fn part1() -> i64 {
    // let input = read_input!('\n', ',', u32);
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let grid = input
        .iter()
        .fold(vec![[false; 1000]; 1000], |mut grid, instruction| {
            for x in instruction.start.x..=instruction.end.x {
                for y in instruction.start.y..=instruction.end.y {
                    match instruction.action {
                        Action::TurnOn => grid[x as usize][y as usize] = true,
                        Action::TurnOff => grid[x as usize][y as usize] = false,
                        Action::Toggle => {
                            grid[x as usize][y as usize] = !grid[x as usize][y as usize]
                        }
                    }
                }
            }
            grid
        });

    grid.iter().flatten().filter(|&&x| x).count() as i64
}

pub fn part2() -> i64 {
    let input = parser::InstructionsParser::new()
        .parse(&get_input!())
        .unwrap();

    let grid = input
        .iter()
        .fold(vec![[0_u8; 1000]; 1000], |mut grid, instruction| {
            for x in instruction.start.x..=instruction.end.x {
                for y in instruction.start.y..=instruction.end.y {
                    match instruction.action {
                        Action::TurnOn => grid[x as usize][y as usize] += 1,
                        Action::TurnOff => {
                            grid[x as usize][y as usize] = if grid[x as usize][y as usize] == 0 {
                                0
                            } else {
                                grid[x as usize][y as usize] - 1
                            }
                        }
                        Action::Toggle => grid[x as usize][y as usize] += 2,
                    }
                }
            }
            grid
        });

    grid.iter().flatten().map(|x| *x as i64).sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 377891);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 14110788);
    }
}
