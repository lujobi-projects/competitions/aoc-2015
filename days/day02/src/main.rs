use utils::*;

const CURRENT_DAY: u8 = 1;

pub fn part1() -> i64 {
    let input = read_input!('\n', 'x', i64);
    input
        .iter()
        .map(|x| {
            if let [l, w, h] = x.as_slice() {
                2 * l * w + 2 * w * h + 2 * h * l + (l * w).min(w * h).min(h * l)
            } else {
                0
            }
        })
        .sum()
}

pub fn part2() -> i64 {
    let input = read_input!('\n', 'x', i64);
    input
        .iter()
        .map(|x| {
            if let [l, w, h] = x.as_slice() {
                l * w * h + 2 * (l + w).min(w + h).min(h + l)
            } else {
                0
            }
        })
        .sum()
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1588178);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 3783758);
    }
}
