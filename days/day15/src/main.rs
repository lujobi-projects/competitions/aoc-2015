use utils::*;

const CURRENT_DAY: u8 = 15;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Ingredient {
    capacity: i32,
    durability: i32,
    flavor: i32,
    texture: i32,
    calories: i32,
}

fn parse_input(input: String) -> Vec<Ingredient> {
    input
        .lines()
        .map(|line| {
            let mut parts = line.split(' ');
            let capacity = parts.nth(2).unwrap().trim_end_matches(',').parse().unwrap();
            let durability = parts.nth(1).unwrap().trim_end_matches(',').parse().unwrap();
            let flavor = parts.nth(1).unwrap().trim_end_matches(',').parse().unwrap();
            let texture = parts.nth(1).unwrap().trim_end_matches(',').parse().unwrap();
            let calories = parts.nth(1).unwrap().parse().unwrap();
            Ingredient {
                capacity,
                durability,
                flavor,
                texture,
                calories,
            }
        })
        .collect()
}

pub fn part1() -> i64 {
    let input = parse_input(get_input!());
    let max_tsp = 100;
    let mut max_ct = 0;

    for tsp1 in 0..=max_tsp {
        for tsp2 in 0..=max_tsp - tsp1 {
            for tsp3 in 0..=max_tsp - tsp1 - tsp2 {
                let tsp4 = max_tsp - tsp1 - tsp2 - tsp3;
                let capacity = tsp1 * input[0].capacity
                    + tsp2 * input[1].capacity
                    + tsp3 * input[2].capacity
                    + tsp4 * input[3].capacity;
                let durability = tsp1 * input[0].durability
                    + tsp2 * input[1].durability
                    + tsp3 * input[2].durability
                    + tsp4 * input[3].durability;
                let flavor = tsp1 * input[0].flavor
                    + tsp2 * input[1].flavor
                    + tsp3 * input[2].flavor
                    + tsp4 * input[3].flavor;
                let texture = tsp1 * input[0].texture
                    + tsp2 * input[1].texture
                    + tsp3 * input[2].texture
                    + tsp4 * input[3].texture;
                let ct = capacity.max(0) * durability.max(0) * flavor.max(0) * texture.max(0);
                max_ct = max_ct.max(ct);
            }
        }
    }

    max_ct as i64
}
pub fn part2() -> i64 {
    let input = parse_input(get_input!());
    let max_tsp = 100;
    let mut max_ct = 0;

    for tsp1 in 0..=max_tsp {
        for tsp2 in 0..=max_tsp - tsp1 {
            for tsp3 in 0..=max_tsp - tsp1 - tsp2 {
                let tsp4 = max_tsp - tsp1 - tsp2 - tsp3;
                let capacity = tsp1 * input[0].capacity
                    + tsp2 * input[1].capacity
                    + tsp3 * input[2].capacity
                    + tsp4 * input[3].capacity;
                let durability = tsp1 * input[0].durability
                    + tsp2 * input[1].durability
                    + tsp3 * input[2].durability
                    + tsp4 * input[3].durability;
                let flavor = tsp1 * input[0].flavor
                    + tsp2 * input[1].flavor
                    + tsp3 * input[2].flavor
                    + tsp4 * input[3].flavor;
                let texture = tsp1 * input[0].texture
                    + tsp2 * input[1].texture
                    + tsp3 * input[2].texture
                    + tsp4 * input[3].texture;
                let calories = tsp1 * input[0].calories
                    + tsp2 * input[1].calories
                    + tsp3 * input[2].calories
                    + tsp4 * input[3].calories;
                if calories != 500 {
                    continue;
                }
                let ct = capacity.max(0) * durability.max(0) * flavor.max(0) * texture.max(0);
                max_ct = max_ct.max(ct);
            }
        }
    }

    max_ct as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 13882464);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 11171160);
    }
}
