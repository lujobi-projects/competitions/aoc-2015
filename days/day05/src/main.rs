use utils::*;

const CURRENT_DAY: u8 = 5;

pub fn part1() -> i64 {
    let input = read_input!("\n", String);
    // let input = debug_input!("ugknbfddgicrmopn", "\n", String);

    input
        .into_iter()
        .filter(|x| {
            x.chars()
                .filter(|c| *c == 'a' || *c == 'e' || *c == 'i' || *c == 'o' || *c == 'u')
                .count()
                >= 3
                && x.chars().zip(x.chars().skip(1)).any(|(a, b)| a == b)
                && !x.contains("ab")
                && !x.contains("cd")
                && !x.contains("pq")
                && !x.contains("xy")
        })
        .count() as i64
}

pub fn part2() -> i64 {
    let input = read_input!("\n", String);
    // let input = debug_input!("");

    input
        .into_iter()
        .filter(|x| {
            x.chars()
                .zip(x.chars().skip(1))
                .enumerate()
                .any(|(i, (a, b))| x[i + 2..].contains(&format!("{}{}", a, b)))
                && x.chars().zip(x.chars().skip(2)).any(|(a, b)| a == b)
        })
        .count() as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 236);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 51);
    }
}
