#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub parser); // synthesized by LALRPOP

use utils::*;

const CURRENT_DAY: u8 = 8;

#[derive(Debug)]
pub enum EscToken {
    Character(String),
    Hex(u8),
    Backslash,
    Quote,
}

pub fn part1() -> i64 {
    let input = parser::tokensParser::new().parse(&get_input!()).unwrap();
    let inputchars = read_input!("\n", String);
    // count all chars inputchars
    let mut count = 0;
    for line in inputchars {
        count += line.len();
    }
    (count - input.iter().flatten().count()) as i64
}

pub fn part2() -> i64 {
    let input = parser::tokensParser::new().parse(&get_input!()).unwrap();
    let inputchars = read_input!("\n", String);
    let mut count = 0;
    for line in inputchars {
        count += line.len();
    }
    println!("{:?}", count);
    input
        .iter()
        .map(|t| {
            t.iter()
                .map(|c| match c {
                    EscToken::Character(_) => 1,
                    EscToken::Hex(_) => 5,
                    EscToken::Backslash => 4,
                    EscToken::Quote => 4,
                })
                .sum::<i64>()
                + 6
        })
        .sum::<i64>()
        - count as i64
}

fn main() {
    print_answer!(CURRENT_DAY, 1, part1());
    print_answer!(CURRENT_DAY, 2, part2());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(part1(), 1371);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(), 2117);
    }
}
