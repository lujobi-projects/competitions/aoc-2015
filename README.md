# Advent of Code 2015

This repository contains my solutions to the [Advent of Code 2015](http://adventofcode.com/2015) challenges.

## Usage

There is a directory for every day in the days directory. Each day has is a single cargo crate and are added to the workspace. The crates are named `dayXX` where `XX` is the day number.

To run the solution for a day, run `cargo run` in the respective directory.

New solutions can be created by running `cargo run --bin newday XX [parsed]` in the root of the repository. This will create a new directory `dayXX` and a new crate `dayXX` in the workspace. If the `parsed` argument is given, the crate will be created with the setup for a lalrpop parser.

## Tests

To run the tests for a day, run `cargo test` in the respective directory. Alternatively you can run `cargo test --all` in the root of the repository to run all tests.
